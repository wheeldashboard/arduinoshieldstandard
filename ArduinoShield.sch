EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Arduino_Leonardo A0
U 1 1 60F41FFC
P 6000 4050
F 0 "A0" V 5900 3900 50  0000 C CNN
F 1 "Arduino_Leonardo" V 6000 3900 50  0000 C CNN
F 2 "Module:Arduino_UNO_R3" H 6000 4050 50  0001 C CIN
F 3 "https://www.arduino.cc/en/Main/ArduinoBoardLeonardo" H 6000 4050 50  0001 C CNN
	1    6000 4050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW0
U 1 1 60F54711
P 5350 3250
F 0 "SW0" H 5400 3350 50  0000 R CNN
F 1 "SW_Push" H 5900 3250 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 5350 3450 50  0001 C CNN
F 3 "~" H 5350 3450 50  0001 C CNN
	1    5350 3250
	0    -1   -1   0   
$EndComp
NoConn ~ 5900 3050
NoConn ~ 6100 3050
$Comp
L Device:R R0
U 1 1 60F5BB8A
P 5350 3600
F 0 "R0" V 5350 3550 50  0000 C CNN
F 1 "R" V 5350 3650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 5280 3600 50  0001 C CNN
F 3 "~" H 5350 3600 50  0001 C CNN
	1    5350 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 5350 6100 5150
NoConn ~ 5900 5150
NoConn ~ 6000 5150
$Comp
L power:GND #PWR00
U 1 1 60F69C11
P 6100 5350
F 0 "#PWR00" H 6100 5100 50  0001 C CNN
F 1 "GND" H 6105 5177 50  0000 C CNN
F 2 "" H 6100 5350 50  0001 C CNN
F 3 "" H 6100 5350 50  0001 C CNN
	1    6100 5350
	1    0    0    -1  
$EndComp
Connection ~ 6100 5350
$Comp
L power:PWR_FLAG #FLG00
U 1 1 60F6C1EB
P 6500 5350
F 0 "#FLG00" H 6500 5425 50  0001 C CNN
F 1 "PWR_FLAG" H 6500 5523 50  0000 C CNN
F 2 "" H 6500 5350 50  0001 C CNN
F 3 "~" H 6500 5350 50  0001 C CNN
	1    6500 5350
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 60F8B07D
P 5150 3700
F 0 "R1" V 5150 3650 50  0000 C CNN
F 1 "R" V 5150 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 5080 3700 50  0001 C CNN
F 3 "~" H 5150 3700 50  0001 C CNN
	1    5150 3700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 60F8B077
P 5150 3350
F 0 "SW1" H 5200 3450 50  0000 R CNN
F 1 "SW_Push" H 5800 3350 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 5150 3550 50  0001 C CNN
F 3 "~" H 5150 3550 50  0001 C CNN
	1    5150 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5500 3450 5350 3450
Connection ~ 5350 3450
Wire Wire Line
	5500 3550 5150 3550
Connection ~ 5150 3550
$Comp
L Switch:SW_Push SW2
U 1 1 60FAE00C
P 4950 3450
F 0 "SW2" H 5000 3550 50  0000 R CNN
F 1 "SW_Push" H 5600 3450 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 4950 3650 50  0001 C CNN
F 3 "~" H 4950 3650 50  0001 C CNN
	1    4950 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 60FAE012
P 4950 3800
F 0 "R2" V 4950 3750 50  0000 C CNN
F 1 "R" V 4950 3850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 4880 3800 50  0001 C CNN
F 3 "~" H 4950 3800 50  0001 C CNN
	1    4950 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 60FAE018
P 4750 3900
F 0 "R3" V 4750 3850 50  0000 C CNN
F 1 "R" V 4750 3950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 4680 3900 50  0001 C CNN
F 3 "~" H 4750 3900 50  0001 C CNN
	1    4750 3900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 60FAE01E
P 4750 3550
F 0 "SW3" H 4800 3650 50  0000 R CNN
F 1 "SW_Push" H 5400 3550 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 4750 3750 50  0001 C CNN
F 3 "~" H 4750 3750 50  0001 C CNN
	1    4750 3550
	0    -1   -1   0   
$EndComp
Connection ~ 4750 3750
Wire Wire Line
	5300 3750 5300 3700
Wire Wire Line
	5300 3700 5400 3700
Wire Wire Line
	5400 3700 5400 3750
Wire Wire Line
	5400 3750 5500 3750
Wire Wire Line
	4750 3750 5300 3750
Wire Wire Line
	4950 3650 5500 3650
Connection ~ 4950 3650
$Comp
L Device:R R4
U 1 1 60FC72FC
P 4550 4000
F 0 "R4" V 4550 3950 50  0000 C CNN
F 1 "R" V 4550 4050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 4480 4000 50  0001 C CNN
F 3 "~" H 4550 4000 50  0001 C CNN
	1    4550 4000
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 60FC7302
P 4550 3650
F 0 "SW4" H 4600 3750 50  0000 R CNN
F 1 "SW_Push" H 5200 3650 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 4550 3850 50  0001 C CNN
F 3 "~" H 4550 3850 50  0001 C CNN
	1    4550 3650
	0    -1   -1   0   
$EndComp
Connection ~ 4550 3850
Wire Wire Line
	5100 3850 5100 3800
Wire Wire Line
	5100 3800 5200 3800
Wire Wire Line
	5200 3800 5200 3850
Wire Wire Line
	4550 3850 5100 3850
$Comp
L Device:R R5
U 1 1 60FC7F3A
P 4350 4100
F 0 "R5" V 4350 4050 50  0000 C CNN
F 1 "R" V 4350 4150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 4280 4100 50  0001 C CNN
F 3 "~" H 4350 4100 50  0001 C CNN
	1    4350 4100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 60FC7F40
P 4350 3750
F 0 "SW5" H 4400 3850 50  0000 R CNN
F 1 "SW_Push" H 5000 3750 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 4350 3950 50  0001 C CNN
F 3 "~" H 4350 3950 50  0001 C CNN
	1    4350 3750
	0    -1   -1   0   
$EndComp
Connection ~ 4350 3950
Wire Wire Line
	4900 3950 4900 3900
Wire Wire Line
	4900 3900 5000 3900
Wire Wire Line
	5000 3900 5000 3950
Wire Wire Line
	4350 3950 4900 3950
$Comp
L Device:R R6
U 1 1 60FCC788
P 4150 4200
F 0 "R6" V 4150 4150 50  0000 C CNN
F 1 "R" V 4150 4250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 4080 4200 50  0001 C CNN
F 3 "~" H 4150 4200 50  0001 C CNN
	1    4150 4200
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 60FCC78E
P 4150 3850
F 0 "SW6" H 4200 3950 50  0000 R CNN
F 1 "SW_Push" H 4800 3850 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 4150 4050 50  0001 C CNN
F 3 "~" H 4150 4050 50  0001 C CNN
	1    4150 3850
	0    -1   -1   0   
$EndComp
Connection ~ 4150 4050
Wire Wire Line
	4700 4050 4700 4000
Wire Wire Line
	4700 4000 4800 4000
Wire Wire Line
	4800 4000 4800 4050
Wire Wire Line
	4150 4050 4700 4050
$Comp
L Device:R R7
U 1 1 60FCDE80
P 3950 4300
F 0 "R7" V 3950 4250 50  0000 C CNN
F 1 "R" V 3950 4350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 3880 4300 50  0001 C CNN
F 3 "~" H 3950 4300 50  0001 C CNN
	1    3950 4300
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 60FCDE86
P 3950 3950
F 0 "SW7" H 4000 4050 50  0000 R CNN
F 1 "SW_Push" H 4600 3950 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 3950 4150 50  0001 C CNN
F 3 "~" H 3950 4150 50  0001 C CNN
	1    3950 3950
	0    -1   -1   0   
$EndComp
Connection ~ 3950 4150
Wire Wire Line
	4500 4150 4500 4100
Wire Wire Line
	4500 4100 4600 4100
Wire Wire Line
	4600 4100 4600 4150
Wire Wire Line
	3950 4150 4500 4150
$Comp
L Device:R R8
U 1 1 60FCF131
P 3750 4400
F 0 "R8" V 3750 4350 50  0000 C CNN
F 1 "R" V 3750 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 3680 4400 50  0001 C CNN
F 3 "~" H 3750 4400 50  0001 C CNN
	1    3750 4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 60FCF137
P 3750 4050
F 0 "SW8" H 3800 4150 50  0000 R CNN
F 1 "SW_Push" H 4400 4050 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 3750 4250 50  0001 C CNN
F 3 "~" H 3750 4250 50  0001 C CNN
	1    3750 4050
	0    -1   -1   0   
$EndComp
Connection ~ 3750 4250
Wire Wire Line
	4300 4250 4300 4200
Wire Wire Line
	4300 4200 4400 4200
Wire Wire Line
	4400 4200 4400 4250
Wire Wire Line
	3750 4250 4300 4250
Wire Wire Line
	5200 3850 5500 3850
Wire Wire Line
	5000 3950 5500 3950
Wire Wire Line
	4800 4050 5500 4050
Wire Wire Line
	5350 3750 5350 3900
Wire Wire Line
	5350 3900 5150 3900
Wire Wire Line
	5150 3900 5150 3850
Wire Wire Line
	5150 3900 5150 4000
Wire Wire Line
	5150 4000 4950 4000
Wire Wire Line
	4950 4000 4950 3950
Connection ~ 5150 3900
Wire Wire Line
	4950 4000 4950 4100
Wire Wire Line
	4950 4100 4750 4100
Wire Wire Line
	4750 4100 4750 4050
Connection ~ 4950 4000
Wire Wire Line
	4600 4150 5500 4150
Wire Wire Line
	4750 4100 4750 4200
Wire Wire Line
	4750 4200 4550 4200
Wire Wire Line
	4550 4200 4550 4150
Connection ~ 4750 4100
Wire Wire Line
	4400 4250 5500 4250
Wire Wire Line
	4550 4200 4550 4300
Wire Wire Line
	4550 4300 4350 4300
Wire Wire Line
	4350 4300 4350 4250
Connection ~ 4550 4200
Wire Wire Line
	3750 3850 3750 3750
Wire Wire Line
	3750 3750 3950 3750
Wire Wire Line
	3950 3750 3950 3650
Wire Wire Line
	3950 3650 4150 3650
Connection ~ 3950 3750
Wire Wire Line
	4350 3550 4150 3550
Wire Wire Line
	4150 3550 4150 3650
Connection ~ 4150 3650
Wire Wire Line
	4350 3550 4350 3450
Wire Wire Line
	4350 3450 4550 3450
Connection ~ 4350 3550
Wire Wire Line
	4550 3450 4550 3350
Wire Wire Line
	4550 3350 4750 3350
Connection ~ 4550 3450
Wire Wire Line
	4750 3350 4750 3250
Wire Wire Line
	4750 3250 4950 3250
Connection ~ 4750 3350
Wire Wire Line
	4950 3250 4950 3150
Wire Wire Line
	4950 3150 5150 3150
Connection ~ 4950 3250
Wire Wire Line
	5150 3150 5150 3050
Wire Wire Line
	5150 3050 5350 3050
Connection ~ 5150 3150
Wire Wire Line
	6200 2800 6200 3050
$Comp
L Device:R R9
U 1 1 61006B87
P 3550 4500
F 0 "R9" V 3550 4450 50  0000 C CNN
F 1 "R" V 3550 4550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 3480 4500 50  0001 C CNN
F 3 "~" H 3550 4500 50  0001 C CNN
	1    3550 4500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW9
U 1 1 61006B8D
P 3550 4150
F 0 "SW9" H 3600 4250 50  0000 R CNN
F 1 "SW_Push" H 4200 4150 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 3550 4350 50  0001 C CNN
F 3 "~" H 3550 4350 50  0001 C CNN
	1    3550 4150
	0    -1   -1   0   
$EndComp
Connection ~ 3550 4350
Wire Wire Line
	3550 4350 4100 4350
$Comp
L Device:R R10
U 1 1 61006B95
P 3350 4600
F 0 "R10" V 3350 4550 50  0000 C CNN
F 1 "R" V 3350 4650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 3280 4600 50  0001 C CNN
F 3 "~" H 3350 4600 50  0001 C CNN
	1    3350 4600
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW10
U 1 1 61006B9B
P 3350 4250
F 0 "SW10" H 3450 4350 50  0000 R CNN
F 1 "SW_Push" H 4000 4250 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 3350 4450 50  0001 C CNN
F 3 "~" H 3350 4450 50  0001 C CNN
	1    3350 4250
	0    -1   -1   0   
$EndComp
Connection ~ 3350 4450
Wire Wire Line
	3350 4450 3900 4450
$Comp
L Device:R R11
U 1 1 61006BA3
P 3150 4700
F 0 "R11" V 3150 4650 50  0000 C CNN
F 1 "R" V 3150 4750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 3080 4700 50  0001 C CNN
F 3 "~" H 3150 4700 50  0001 C CNN
	1    3150 4700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW11
U 1 1 61006BA9
P 3150 4350
F 0 "SW11" H 3250 4450 50  0000 R CNN
F 1 "SW_Push" H 3800 4350 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 3150 4550 50  0001 C CNN
F 3 "~" H 3150 4550 50  0001 C CNN
	1    3150 4350
	0    -1   -1   0   
$EndComp
Connection ~ 3150 4550
Wire Wire Line
	3150 4550 3700 4550
$Comp
L Device:R R12
U 1 1 61006BB1
P 2950 4800
F 0 "R12" V 2950 4750 50  0000 C CNN
F 1 "R" V 2950 4850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 2880 4800 50  0001 C CNN
F 3 "~" H 2950 4800 50  0001 C CNN
	1    2950 4800
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW12
U 1 1 61006BB7
P 2950 4450
F 0 "SW12" H 3050 4550 50  0000 R CNN
F 1 "SW_Push" H 3600 4450 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 2950 4650 50  0001 C CNN
F 3 "~" H 2950 4650 50  0001 C CNN
	1    2950 4450
	0    -1   -1   0   
$EndComp
Connection ~ 2950 4650
Wire Wire Line
	3500 4650 3500 4600
Wire Wire Line
	3500 4600 3600 4600
Wire Wire Line
	3600 4600 3600 4650
Wire Wire Line
	2950 4650 3500 4650
$Comp
L Device:R R13
U 1 1 61006BC2
P 2750 4900
F 0 "R13" V 2750 4850 50  0000 C CNN
F 1 "R" V 2750 4950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 2680 4900 50  0001 C CNN
F 3 "~" H 2750 4900 50  0001 C CNN
	1    2750 4900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW13
U 1 1 61006BC8
P 2750 4550
F 0 "SW13" H 2850 4650 50  0000 R CNN
F 1 "SW_Push" H 3400 4550 50  0001 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.8mm_D0.9mm_OD2.3mm" H 2750 4750 50  0001 C CNN
F 3 "~" H 2750 4750 50  0001 C CNN
	1    2750 4550
	0    -1   -1   0   
$EndComp
Connection ~ 2750 4750
Wire Wire Line
	3300 4750 3300 4700
Wire Wire Line
	3300 4700 3400 4700
Wire Wire Line
	3400 4700 3400 4750
Wire Wire Line
	2750 4750 3300 4750
Wire Wire Line
	3750 4700 3550 4700
Wire Wire Line
	3550 4700 3550 4650
Wire Wire Line
	3550 4700 3550 4800
Wire Wire Line
	3550 4800 3350 4800
Wire Wire Line
	3350 4800 3350 4750
Connection ~ 3550 4700
Wire Wire Line
	2750 4350 2750 4250
Wire Wire Line
	2750 4250 2950 4250
Wire Wire Line
	2950 4250 2950 4150
Wire Wire Line
	2950 4150 3150 4150
Connection ~ 2950 4250
Wire Wire Line
	3350 4050 3150 4050
Wire Wire Line
	3150 4050 3150 4150
Connection ~ 3150 4150
Wire Wire Line
	3350 4050 3350 3950
Wire Wire Line
	3350 3950 3550 3950
Connection ~ 3350 4050
Wire Wire Line
	3550 3950 3550 3850
Connection ~ 3550 3950
Wire Wire Line
	3400 4750 5500 4750
Wire Wire Line
	3600 4650 5500 4650
Wire Wire Line
	3700 4550 3700 4500
Wire Wire Line
	3700 4500 3800 4500
Wire Wire Line
	3800 4500 3800 4550
Wire Wire Line
	3800 4550 5500 4550
Wire Wire Line
	5500 4450 4000 4450
Wire Wire Line
	4000 4450 4000 4400
Wire Wire Line
	4000 4400 3900 4400
Wire Wire Line
	3900 4400 3900 4450
Wire Wire Line
	4100 4350 4100 4300
Wire Wire Line
	4100 4300 4200 4300
Wire Wire Line
	4200 4300 4200 4350
Wire Wire Line
	4200 4350 5500 4350
Wire Wire Line
	4350 4300 4350 4400
Wire Wire Line
	4350 4400 4150 4400
Wire Wire Line
	4150 4400 4150 4350
Connection ~ 4350 4300
Wire Wire Line
	4150 4400 4150 4500
Wire Wire Line
	4150 4500 3950 4500
Wire Wire Line
	3950 4500 3950 4450
Connection ~ 4150 4400
Wire Wire Line
	3950 4500 3950 4600
Wire Wire Line
	3950 4600 3750 4600
Wire Wire Line
	3750 4600 3750 4550
Connection ~ 3950 4500
Wire Wire Line
	3750 4600 3750 4700
Connection ~ 3750 4600
Wire Wire Line
	3350 4800 3350 4850
Wire Wire Line
	3350 4850 3150 4850
Connection ~ 3350 4800
Wire Wire Line
	3150 4850 3150 4950
Wire Wire Line
	3150 4950 2950 4950
Connection ~ 3150 4850
Wire Wire Line
	2750 5050 2950 5050
Wire Wire Line
	2950 5050 2950 4950
Connection ~ 2950 4950
Wire Wire Line
	2750 5050 2750 5350
Wire Wire Line
	3550 3850 3750 3850
Connection ~ 3750 3850
NoConn ~ 6500 4850
NoConn ~ 6500 4750
NoConn ~ 6500 3850
NoConn ~ 6500 3650
NoConn ~ 6500 3450
Wire Wire Line
	6500 4050 6600 4050
Wire Wire Line
	6500 4250 7100 4250
Wire Wire Line
	7350 4350 6500 4350
Wire Wire Line
	6750 4200 6750 4300
Wire Wire Line
	6750 4300 7000 4300
Wire Wire Line
	7000 4400 7250 4400
Wire Wire Line
	7000 4400 7000 4300
Wire Wire Line
	7250 4400 7250 4500
Wire Wire Line
	7250 4500 7500 4500
Wire Wire Line
	7500 4500 7500 4600
Wire Wire Line
	7500 4200 7500 4100
Wire Wire Line
	7500 4100 7250 4100
Wire Wire Line
	7250 4100 7250 4000
Wire Wire Line
	7250 4000 7000 4000
Wire Wire Line
	7000 4000 7000 3900
Connection ~ 6500 5350
Wire Wire Line
	6500 5350 6100 5350
Connection ~ 2750 5050
Wire Wire Line
	2750 5350 6100 5350
Wire Wire Line
	7000 3900 6750 3900
Wire Wire Line
	6500 4450 7600 4450
Wire Wire Line
	7850 4550 6500 4550
Wire Wire Line
	7500 4600 7750 4600
Wire Wire Line
	7750 4600 7750 4700
Wire Wire Line
	7750 4700 8000 4700
Wire Wire Line
	8000 4700 8000 5350
Wire Wire Line
	8000 5350 6500 5350
Wire Wire Line
	7500 4200 7750 4200
Wire Wire Line
	7750 4200 7750 4300
Wire Wire Line
	7750 4300 8000 4300
Wire Wire Line
	8000 4300 8000 4400
Wire Wire Line
	6600 2800 6200 2800
Wire Wire Line
	5350 3050 5450 3050
Wire Wire Line
	5450 3050 5450 2800
Wire Wire Line
	5450 2800 6200 2800
Connection ~ 5350 3050
Connection ~ 6200 2800
Wire Wire Line
	6750 3900 6750 3800
Wire Wire Line
	6750 3800 6600 3800
Wire Wire Line
	6600 3800 6600 2800
$Comp
L Device:R_POT RV0
U 1 1 61295FD7
P 6750 4050
F 0 "RV0" H 6680 4004 50  0000 R CNN
F 1 "R_POT" H 6680 4095 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x03_P4.6mm_D0.9mm_OD2.1mm" H 6750 4050 50  0001 C CNN
F 3 "~" H 6750 4050 50  0001 C CNN
	1    6750 4050
	-1   0    0    1   
$EndComp
Connection ~ 6750 3900
$Comp
L Device:R_POT RV1
U 1 1 61299B82
P 7000 4150
F 0 "RV1" H 6930 4104 50  0000 R CNN
F 1 "R_POT" H 6930 4195 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x03_P4.6mm_D0.9mm_OD2.1mm" H 7000 4150 50  0001 C CNN
F 3 "~" H 7000 4150 50  0001 C CNN
	1    7000 4150
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT RV2
U 1 1 612A0D53
P 7250 4250
F 0 "RV2" H 7180 4204 50  0000 R CNN
F 1 "R_POT" H 7180 4295 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x03_P4.6mm_D0.9mm_OD2.1mm" H 7250 4250 50  0001 C CNN
F 3 "~" H 7250 4250 50  0001 C CNN
	1    7250 4250
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT RV3
U 1 1 612A7FD5
P 7500 4350
F 0 "RV3" H 7430 4304 50  0000 R CNN
F 1 "R_POT" H 7430 4395 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x03_P4.6mm_D0.9mm_OD2.1mm" H 7500 4350 50  0001 C CNN
F 3 "~" H 7500 4350 50  0001 C CNN
	1    7500 4350
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT RV4
U 1 1 612AF266
P 7750 4450
F 0 "RV4" H 7680 4404 50  0000 R CNN
F 1 "R_POT" H 7680 4495 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x03_P4.6mm_D0.9mm_OD2.1mm" H 7750 4450 50  0001 C CNN
F 3 "~" H 7750 4450 50  0001 C CNN
	1    7750 4450
	-1   0    0    1   
$EndComp
$Comp
L Device:R_POT RV5
U 1 1 612B6494
P 8000 4550
F 0 "RV5" H 7930 4504 50  0000 R CNN
F 1 "R_POT" H 7930 4595 50  0000 R CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x03_P4.6mm_D0.9mm_OD2.1mm" H 8000 4550 50  0001 C CNN
F 3 "~" H 8000 4550 50  0001 C CNN
	1    8000 4550
	-1   0    0    1   
$EndComp
Wire Wire Line
	6500 4150 6850 4150
$EndSCHEMATC
